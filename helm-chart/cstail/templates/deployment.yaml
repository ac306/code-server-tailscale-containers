{{- if not .Values.noDeploy }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "code-server-tailscale-ubuntu.fullname" . }}
  labels:
    {{- include "code-server-tailscale-ubuntu.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "code-server-tailscale-ubuntu.selectorLabels" . | nindent 6 }}
  strategy:
    type: Recreate
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "code-server-tailscale-ubuntu.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "code-server-tailscale-ubuntu.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
      - name: check-existing-machine
        image: "{{ .Values.codeServerContainer.image.repository }}:{{ .Values.codeServerContainer.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.codeServerContainer.image.pullPolicy }}
        env:
        - name: TS_TAILNET_NAME
          value: "{{ .Values.tailnetConfig.tailnetName }}"
        - name: TS_HOSTNAME
          value: "{{ include "code-server-tailscale-ubuntu.tailnetHostname" . }}"
        - name: TS_API_KEY
          valueFrom:
            secretKeyRef:
              name: {{ include "code-server-tailscale-ubuntu.secretName" . }}
              key: TS_API_KEY
              optional: false
        command:
        - "/bin/bash"
        - -c
        - |
            echo "Checking if another host with the same hostname in the tailnet exists"
            TS_HOST=$(echo "$TS_HOSTNAME.$TS_TAILNET_NAME" | sed -e "s/@/./g")
            MACHINE_ID=$(curl 'https://api.tailscale.com/api/v2/tailnet/'$TS_TAILNET_NAME'/devices' -u "$TS_API_KEY:" | jq '.devices[] | select(.name == "'$TS_HOST'") .id' | sed -e 's/"//g')
            if [ ! -z "$MACHINE_ID" ]; then echo "Old machine already exists with id $MACHINE_ID; sleeping for 30 seconds to wait for termination"; sleep 30; fi
            MACHINE_ID=$(curl 'https://api.tailscale.com/api/v2/tailnet/'$TS_TAILNET_NAME'/devices' -u "$TS_API_KEY:" | jq '.devices[] | select(.name == "'$TS_HOST'") .id' | sed -e 's/"//g')
            if [ ! -z "$MACHINE_ID" ]; then echo "Device still exists; assuming it will not be cleaned up - deleting" ; curl -X DELETE 'https://api.tailscale.com/api/v2/device/'$MACHINE_ID'' -u "$TS_API_KEY:"; fi
            sleep 5
      containers:
      - name: tailscale-sidecar
        image: "{{ .Values.tailscaleSidecarContainer.image.repository }}:{{ .Values.tailscaleSidecarContainer.image.tag }}"
        imagePullPolicy: "{{ .Values.tailscaleSidecarContainer.image.pullPolicy }}"
        resources:
          {{- toYaml .Values.tailscaleSidecarContainer.resources | nindent 10 }}
        env:
        - name: TS_KUBE_SECRET
          value: {{ include "code-server-tailscale-ubuntu.secretName" . }}
        - name: TS_USERSPACE
          value: "false"
        - name: TS_STATE_DIR
          value: "/var/lib/tailscale/data"
        - name: TS_AUTH_KEY
          valueFrom:
            secretKeyRef:
              name: {{ include "code-server-tailscale-ubuntu.secretName" . }}
              key: TS_AUTH_KEY
              optional: false
        - name: TS_HOSTNAME
          value: {{ include "code-server-tailscale-ubuntu.tailnetHostname" . }}
        - name: TS_TAILSCALED_EXTRA_ARGS
          value: " --statedir=$(TS_STATE_DIR) "
        - name: TS_EXTRA_ARGS
          value: " --hostname=$(TS_HOSTNAME) "
        volumeMounts:
        - mountPath: /var/lib/tailscale/data
          name: tailscale-data
        - mountPath: /tmp
          name: tailscaled-socket
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
      - name: {{ .Chart.Name }}
        securityContext:
          {{- toYaml .Values.securityContext | nindent 10 }}
        image: "{{ .Values.codeServerContainer.image.repository }}:{{ .Values.codeServerContainer.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.codeServerContainer.image.pullPolicy }}
        ports:
        - name: cs-https
          containerPort: 8443
          protocol: TCP
        env:
        - name: TS_TAILNET_NAME
          value: "{{ .Values.tailnetConfig.tailnetName }}"
        - name: TS_HOSTNAME
          value: {{ include "code-server-tailscale-ubuntu.tailnetHostname" . }}
        - name: TS_API_KEY
          valueFrom:
            secretKeyRef:
              name: {{ include "code-server-tailscale-ubuntu.secretName" . }}
              key: TS_API_KEY
              optional: false
        # This command supports any OS as long as they have the tailscale agent, jq, grep (with that -P flag thingy), and code-server, openssl, curl, and
        # https://gist.githubusercontent.com/bmatthewshea/2f4301b769a46e7eb10d554a52a864b3/raw/a425cbcf6963ea8e406737ce5b56329eda1438c5/show_ssl_expire
        # installed in them.
        command:
          - "/bin/bash"
          - -c
          - |
            set -x
            echo "Waiting for tailscale"
            sleep 60
            echo "Checking domain"
            TS_DOMAIN=$(tailscale --socket=/tmp/tailscaled.sock cert 2>&1 | grep -o -P '(?<=").*(?=")' | tail -n 1)
            echo $TS_DOMAIN
            echo "Checking tailnet ip"
            TS_ADDR=$(tailscale --socket=/tmp/tailscaled.sock ip -4)
            EXPIRED_CERT=""
            GEN_CERT=""
            if [ -f /code-server/userdata/certificates/$TS_DOMAIN.crt ]; then echo "Existing cert detected; attempting to reuse"; EXPIRED_CERT=$(show_ssl_expire -d 1 -f /code-server/userdata/certificates/$TS_DOMAIN.crt | grep -i '!'); else echo "No existing certificate found; requesting regen"; GEN_CERT="true"; fi
            if [ ! -z "$EXPIRED_CERT" ]; then echo "Existing certificate for code-server has expired; requesting regen"; GEN_CERT="true"; fi
            if [ "$GEN_CERT" == "true" ]; then echo "Generating certificates for code-server"; mkdir -p /code-server/userdata/certificates; tailscale --socket=/tmp/tailscaled.sock cert --cert-file /code-server/userdata/certificates/$TS_DOMAIN.crt --key-file /code-server/userdata/certificates/$TS_DOMAIN.key $TS_DOMAIN; else echo "Reusing old, unexpired certificate"; fi
            echo "Printing config data"
            printf "bind-addr: $TS_ADDR:8443\nauth: none\ncert: /code-server/userdata/certificates/$TS_DOMAIN.crt\ncert-key: /code-server/userdata/certificates/$TS_DOMAIN.key\nuser-data-dir: /code-server/userdata\n" > /tmp/config.yaml
            cat /tmp/config.yaml
            echo "code-server-url: https://$TS_DOMAIN:8443/"
            code-server --config /tmp/config.yaml
        lifecycle:
          preStop:
            exec:
              command:
              - "/bin/bash"
              - -c
              - |
                TS_HOST=$(echo "$TS_HOSTNAME.$TS_TAILNET_NAME" | sed -e "s/@/./g")
                MACHINE_ID=$(curl 'https://api.tailscale.com/api/v2/tailnet/'$TS_TAILNET_NAME'/devices' -u "$TS_API_KEY:" | jq '.devices[] | select(.name == "'$TS_HOST'") .id' | sed -e 's/"//g')
                echo "Attempting to clean up!"
                echo "Host: $TS_HOST"
                echo "MachineId: $MACHINE_ID"
                echo "Deleting device from tailnet"
                curl -X DELETE 'https://api.tailscale.com/api/v2/device/'$MACHINE_ID'' -u "$TS_API_KEY:"
        volumeMounts:
        - name: userdata
          mountPath: /code-server/userdata
        - name: tailscaled-socket
          mountPath: /tmp
        resources:
          {{- toYaml .Values.codeServerContainer.resources | nindent 10 }}
      volumes:
      - name: userdata
        {{- if .Values.volumes.userdataVolume }}
        {{- toYaml .Values.volumes.userdataVolume | nindent 8 }}
        {{- else }}
        emptyDir: {}
        {{- end }}
      - name: tailscale-data
        emptyDir: {}
      - name: tailscaled-socket
        emptyDir: {}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}